*Bob Dinh, 01.Jan 2018 (published 1 January 2018)*

## FOR a, b, c INTEGERS

# Math project: Pythagoras updated, faster pythagoras

1. Disclaimer
     - I DO NOT COPY ANYBOY's ideas, just accidentally find this new law
       - This is not meant to replace pythagoras
          - Just a "fun" project

2. Let's start !
 - What is pythagoras?
   
[Read more about pythagoras](https://en.wikipedia.org/wiki/Pythagorean_theorem) 

###### Starting with old idea:

- (a^2+b^2=c^2) - this formula was found by Pythagoras 

Right triangle: 

![alt text](https://upload.wikimedia.org/wikipedia/commons/thumb/6/6f/Rtriangle.svg/220px-Rtriangle.svg.png)

- to find a, we use: a=√(c^2+b^2) 

____________________________________________________
#### IS THIS THE ONLY WAY? -NO! I've found other ways

> ### Here is how
___________________________________________________________

#### `CONSIDER THESE SPECIAL FORMS: `

**_change 2:_** 

> with unknown a (or b) for c =b (or a) +2
```javascript
a=(c-b)√((c-b)/2)

```
 
**_change 4:_**

> if c= b (or a) +4 

> for unknown a (or b) and b< c >a

```javascript
a = (c-b) √(d)   for c/b = e/f (factored) if (e == f+2) {d = (e+f)/2} else if(e==f+1) {d = e + f} return true if c, b > 8, 4 and c is EVEN

a= (c-b-2) √(d)   for (d = c + b) return true if c is ODD

```

**_change 3:_**

> if c = b (or a) +3

> for unknown a (or b) and b< c >a

```javascript
a= (c-b) √(d) for c/b = e/f => (d=e + f) return true if c%3 == 0 and c >3 else if(c==3) {a=3;}

```
 
**_more extend:_**

 
~~a = √(3*(c+b))  return true if (c%3)! = 0 => pythagoras~~
  - since c = b+3 => 3 = c-b => √((c-b)(c+b))
 _________________________________________________________________

#### `OVERALL FORM (except those special forms): `

# for all numbers: 

> if c= b (or a) + even

> for unknown a (or b) and b< c >a and c >= 6


~~a=2*√(((c-b)/2)^2 +(c-(c-b))*((c-b)/2)) => Pythagoras~~
 
# for other numbers:

> if c= b (or a) + odd

> for unknown a (or b) and b< c >a and c >= 5

~~a= √(d(c+b))  return true if d = c-b => Pythagoras~~
 
# Proving (for special forms):

*change 2:*

c | b | a  | Calc
--| --|----|----
2 | 0 | 2√1 | (2-0)√((2+0)/2)
4 | 2 | 2√3 | (4-2)√(4-2+1)
5 | 3 | 2√4=4 | (5-3)√((5+3)/2)
6 | 4 | 2√5 | (6-4)√((6+4)/2)
7 | 5 | 2√6 | (7-5)√((7+5)/2)
8 | 6 | 2√7 | (8-6)√((8+6)/2)
..| ..| ... | ...

*change 3:*

c | b | a   | Calc       | Explain
--| --|---- |------------|----------
3 | 0 | 3√1 | 3          | law above
9 | 6 | 3√5 |(9-6)√(3+2) | 9/6=3/2
12| 9 | 3√7 |(12-9)√(4+3)| 12/9 = 4/3
..|...|....| ...  ...    |..

*change 4:*

c | b | a   | Calc | Explain
--| --|---- |----- |-----
5 | 1 | 2√6 | (5-1-2)√(5+1)     | ODD
12 | 8 | 2√20=4√5 |(12-8)√(3+2)|12/8=3/2 & 3=3+1 & EVEN
7 | 3 | 2√14 |(7-3-2)√(7+3)     | ODD
10 | 6 | 8 |(10-6)√((5+3)/2) | 10/6=5/3 & 5=3+2 & EVEN
..|...|.....| ... |...

# Proving (for overall forms):
 
 *all numbers*
 
 **_Change 6_**
 
 c | b | a                                     
  --|---|----          
  6 | 0 | 2√9           
7  | 1 | 2√12          
8  | 2 | 2√15                                                 
9  | 3 | 2√18                                              
10 | 4 | 2√21                                      
11 | 5 | 2√24                                   
12 | 6 | 2√27                                                 
...|...|**2√(9+(c-6)3)**        

We can see that they all have "2√" in common so ignore this, we have to find what is going on on the square root. Look at the number from √9 to infinity, they are all add up 3. So, we can find how many numbers from the first number (6) to the given number which is c by subtracting. Then we multiple that by 3(change in all number) and plus 9 (first square root) to find the number in the final square root.


**_Change 8_**

 c | b | a    
 --|---|----                  
 8 | 0 | 2√16              
 9 | 1 | 2√20                 
10 | 2 | 2√24             
11 | 3 | 2√28                          
12 | 4 | 2√32
.. |...| **2√(16+(c-8)4)**             

Similar above
                
**_Change 10_**    
    
 c | b | a     
 --|---|----
 10 | 0  |  2√2
 ...|....| **2√(25+(c-10)5)**
 
 **_Change infinity even_**
 
 => the overall form of change in 8+even is: **2√((c-b)/2)^2+ (c-(c-b)).((c-b)/2)**
 
 sum up to that fomular, th fisrt number is "2√", second number is the first number on the square root which is made by the change between two numbers squared. The third number is the total number form the given number (c) to th last number and fourth number is the square root form of the first number.
 
 > Unfortunately, it is the "complex" version of pythagoras
 
 *other numbers*

**_change 5:_**

c | b | a     
 --|---|----
 5 | 0  | 5
 6 | 1  | √35
 7 | 2  | 3√5 =√45
 ...|....| **a = √(5(c+b))**

 => a = √((c-b)(c+b))
 
 we can see in the example: √35 is made of 5 or (6-1) times 6+1, so does √45. This way of thinking can be use not only change 5, may be 7, etc.
 
 > This is also not-a-factored-form from a = √(c^2 - b^2)
 
# Advantages and disadvantages of using this:
 
# Advantages
 
 New way | Old way
------------ | -------------
Don't have to use calculator | have to use calculator
Faster | not very fast

# Disadvantages

New way | Old way
------------ | -------------
Only INTEGERS | all real numbers
Hard at first | not very hard
 Small range  |  ALL NUMBERS
 modified pythagoras | already pythagoras 
 
## WHAT NEEDS TO BE DONE?
- [x] Basic ideas
- [ ] Dig Deep, new ideas?
- [ ] Final publish

## Interesting things about doing this project:
every number from declared number to ∞; even though i do NOT use pythagoras, everything leads to pythagoras (you can see in the fomular above). By that, i mean pythagoras theorem is the shorten form of those formular and cannot be replaced.

# Conclusion

This is a fun project, i found that the "Middle" numbers ("in change 2" - (c-b)/2 is the middle number between c and b; "in change 3"-(e+f)/2 is the middle number between e and f ) and the sum of 2 numbers (happens when they are consecutive numbers) is very important. Through this, we can understand more about pythagoras theorem, calculate faster without calculator and ideas that pythagoras may not be "monopoly".

# Draft 
Try all numbers you can think of

